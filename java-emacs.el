;; (require 'package)
;; (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
;; (package-initialize)

;; ;; Uncomment this next line if you want line numbers on the left side
;; ;(global-linum-mode 1)
;; (global-set-key "\C-c\C-v" 'compile)
;; (setq line-number-mode t)
;; (setq column-number-mode t)
;; (display-time)
;; (global-font-lock-mode t)
;; (setq font-lock-maximum-decoration t)

;; ;;This makes rainbow delimiters mode the default.
;; ;;comment out to turn it off.
;; (add-hook 'find-file-hook 'rainbow-delimiters-mode-enable)

;; ;;Want electric pair mode? Uncomment the next line
;; (electric-pair-mode)

;; ;;Want to turn off show paren mode? Comment out the below line.
;; (show-paren-mode)
;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(compilation-auto-jump-to-first-error nil)
;;  '(dap-auto-show-output nil)
;;  '(dap-java-test-additional-args (quote ("--include-classname" ".+")))
;;  '(dcoverage-moderate-covered-report-color "dark orange")
;;  '(dcoverage-package-name-elide "edu.duke.ece651.")
;;  '(dcoverage-pooly-covered-report-color "red")
;;  '(dcoverage-well-covered-report-color "green")
;;  '(inhibit-startup-screen t)
;;  '(lsp-ui-doc-alignment (quote window))
;;  '(lsp-ui-doc-enable nil)
;;  '(lsp-ui-doc-include-signature t)
;;  '(lsp-ui-doc-position (quote bottom))
;;  '(lsp-ui-doc-use-webkit nil)
;;  '(lsp-ui-flycheck-enable t)
;;  '(lsp-ui-flycheck-live-reporting t)
;;  '(lsp-ui-imenu-enable t)
;;  '(lsp-ui-peek-always-show t)
;;  '(lsp-ui-sideline-enable nil)
;;  '(package-selected-packages
;;    (quote
;;     (helm-lsp dracula-theme posframe lsp-ui company-lsp lsp-mode groovy-mode forge flycheck-kotlin kotlin-mode magit memoize meghanada lsp-javacomp lsp-java jtags javadoc-lookup java-imports gradle-mode flycheck-popup-tip flycheck-gradle cov company-rtags company-c-headers clang-format)))
;;  '(safe-local-variable-values (quote ((TeX-master . t)))))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(lsp-ui-peek-line-number ((t (:foreground "deep sky blue"))))
;;  '(lsp-ui-peek-selection ((t (:background "blue" :foreground "white smoke"))))
;;  '(lsp-ui-sideline-code-action ((t (:background "black" :foreground "lawn green")))))


;; (global-set-key "\C-x\C-g" 'goto-line)


;; ; Automatically set compilation mode to
;; ; move to an error when you move the point over it
;; ;(add-hook 'compilation-mode-hook
;; ; (lambda () 
;; ;   (progn
;; ;     (next-error-follow-minor-mode))))

;; ;;Automatically go to the first error
;; ;;This works great for C/C++---not so much for Java
;; ;; (results in a lot of trying to find the file, since the full path
;; ;; isn't usually in the error message)
;; ;(setq compilation-auto-jump-to-first-error t)
;; (defun set-window-undedicated-p (window flag)
;;   "Never set window dedicated."
;;   flag)
;; (setq-default indent-tabs-mode nil)
;; (advice-add 'set-window-dedicated-p :override #'set-window-undedicated-p)


;; (require 'company)
;; (require 'company-rtags)
;; (global-company-mode)
;; (require 'clang-format)


;; (add-hook 'c-mode-common-hook
;;           (function (lambda ()
;;                     (add-hook 'write-contents-functions
;;                               (lambda() (progn (clang-format-buffer) nil))))))

;; (add-hook 'cpp-mode-common-hook
;;           (function (lambda ()
;;                       (add-hook 'write-contents-functions
;;                                 (lambda() (progn (clang-format-buffer) nil))))))
;; (add-to-list 'company-backends 'company-c-headers)
;; (require 'flycheck)
;; (global-flycheck-mode)
;; (flycheck-popup-tip-mode)

;; ;;This is from 551, where we had grade.txt in color.
;; ;; (defun colorize-grade-txt ()
;; ;;   "Make a grade.txt file show colors, then set read only."
;; ;;   (interactive)
;; ;;   (if (or (string= (buffer-name) "grade.txt")
;; ;;           (string-prefix-p "grade.txt<" (buffer-name)))
;; ;;       (progn (let ((inhibit-read-only t))
;; ;;                (ansi-color-apply-on-region (point-min) (point-max)))
;; ;;              (set-buffer-modified-p nil)
;; ;;              (read-only-mode t))))

;; ;; (add-hook 'find-file-hook 'colorize-grade-txt)


(add-hook 'gud-mode-hook (lambda() (company-mode 0)))




(require 'gradle-mode)
(require 'lsp-mode)
(require 'company-lsp)
(require 'lsp-ui)
(require 'lsp-java)
(push 'company-lsp company-backends)


(defun find-path-component(path target)
  "Find TARGET in PATH, returning a list of all components found along the way. 
Returns t if TARGET not found."
  (cond ((equal path "") t)
        ((equal path "/") t)
        ((equal target (file-name-nondirectory path)) nil)
        (t (let ((tmp (find-path-component (directory-file-name (file-name-directory path)) target)))
             (if (equal tmp t)
                 t
               (cons (file-name-nondirectory path) tmp))))))

(defun java-smart-class-skel ()
  "Generate a Java class skeleton based on the current path."
  (interactive)

  (let* ((bname (buffer-file-name))
         (cname (file-name-base bname))
         (ctype (find-path-component bname "src"))
         (istest (if (listp ctype) (equal (car (reverse ctype)) "test") nil))
         (pkg (find-path-component bname "java")))
    (if (and (listp pkg)
             (> (length pkg) 1))
        (progn
          (insert "package ")
          (insert (mapconcat 'identity (reverse (cdr pkg)) "."))
          (insert ";\n\n")))
    (if istest
        (progn
          (insert "import static org.junit.jupiter.api.Assertions.*;\n\n")
          (insert "import org.junit.jupiter.api.Test;\n\n")))
    (insert "public class " cname " ")
    (insert "{\n")
    (if istest
          (insert "  @Test\n  public void test_() {\n\n  }\n"))
    (insert "\n")
    (insert "}\n")
    (if istest
        (progn
          (search-backward "public void test_()")
          (search-forward "()")
          (backward-char 2))
      
      (progn
        (goto-char (point-min))
        (search-forward (concat "public class " cname))))))
(require 'hydra)        
(require 'dap-mode)
(require 'dap-java)

(require 'helm-lsp)


(require 'elquery)
(add-to-list 'load-path "~/.emacs.d/dcoverage/")
(require 'dcoverage)
(require 'yasnippet)
(yas-global-mode)
(defun build-and-run ()
  "Single key stroke for gradle to build and run the program."
  (interactive)
  (gradle-run "--info build run"))

(define-key gradle-mode-map (kbd "C-c C-r") 'build-and-run)

(add-to-list 'auto-mode-alist '("build.gradle" . groovy-mode))

(global-set-key "\C-cg" 'magit-status)
(require 'magit)
(require 'forge)
(add-to-list 'forge-alist '("gitlab.oit.duke.edu"  "gitlab.oit.duke.edu/api/v4" "gitlab.oit.duke.edu" forge-gitlab-repository))


(add-hook 'dap-stopped-hook
          (lambda (arg)
            (call-interactively #'dap-hydra)))
(global-set-key "\C-c\C-h" 'hydra-pause-resume)

(add-hook 'dap-terminated-hook
          (lambda (arg)
            (hydra-disable)))


(defun gradle-clean-and-compile ()
  "Run gradle clean classes testClasses."
  (interactive)
  (gradle-run "clean classes testClasses"))

(defun ece651-debug-test-case()
  "Ensure that classes are up to date, copy to bin, and debug current test."
  (interactive)
  ;;we'll make sure everything is up to date
  (let* ((basedir (dcoverage-find-project-root))
         (testdir (concat basedir "build/classes/java/test"))
         (maindir (concat basedir "build/classes/java/main"))
         (bindir (concat basedir "bin/"))
         (testdest (concat bindir "test"))
         (maindest (concat bindir "main")))
    ;;check that build made these before continuing
    (if (not (file-directory-p testdir))
        (error "Test directory %s does not exist (run gradle?)" testdir))
    (if (not (file-directory-p maindir))
        (errro "Main directory %s does not exist (run gradle?)" maindir))
    ;;copy build dirs to bin/
    (delete-directory testdest t)
    (delete-directory maindest t)
    (copy-directory testdir testdest t t nil)
    (copy-directory maindir maindest t t nil)
    ;;now we can run the debugger
    (message "Starting debugger... %s" (current-buffer))
    (dap-breakpoint-add)
    (save-window-excursion (call-interactively 'dap-java-debug-test-method))
    (message "Press C-c C-h to toggle debug hydra")
    (cons "debugger started" "debugging")))
          
(add-hook 'java-mode-hook
          (lambda()
            (flycheck-mode +1)
            (setq c-basic-offset 2)
            (gradle-mode)
            (dap-mode 1)
            (dap-ui-mode 1)
            (dap-tooltip-mode 1)
            (tooltip-mode 1)
            (lsp)
            (local-set-key "\C-c\C-a" 'lsp-java-add-unimplemented-methods)
            (local-set-key "\C-c\C-i" 'lsp-java-organize-imports)
            (local-set-key "\C-c\C-o" 'lsp-java-generate-overrides)
            (local-set-key "\C-c\C-g" 'lsp-java-generate-getters-and-setters)
            (local-set-key "\C-c\C-e" 'lsp-java-extract-method)
            (local-set-key "\C-ci" 'lsp-goto-implementation)
            (local-set-key "\C-ct" 'lsp-goto-type-definition)
            (local-set-key "\C-c\C-j" 'javadoc-lookup)
            (local-set-key "\C-c\C-v" 'gradle-execute)
            (local-set-key "\C-c\C-f" 'lsp-format-buffer)
            (local-set-key "\C-cr" 'lsp-rename)
            (local-set-key "\C-cd" 'lsp-ui-peek-find-definitions)
            (local-set-key "\C-cu" 'lsp-ui-peek-find-references)
            (local-set-key "\C-cx"  'gradle-clean-and-compile)
            (local-set-key "\C-c\C-s" 'java-smart-class-skel)
            (local-set-key "\C-c\C-d" 'ece651-debug-test-case)
            (setq c-basic-offset 2)
            (setq tab-width 2)))
                 

;; (load-theme 'deeper-blue)

