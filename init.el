;;; package ---- Summary
;;; package -- Commentary
;;; Commentary:
;; Pablo's favorite Emacs settings, mostly for C++ and Python programming
;;; Code:
(require 'package)
(setq package-archives
      '(("GNU ELPA"     . "https://elpa.gnu.org/packages/")
      ("MELPA Stable" . "https://stable.melpa.org/packages/")
        ("MELPA"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("GNU ELPA"     . 10)
        ("MELPA Stable" . 5)
        ("MELPA"        . 0)))
(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(auto-complete-clang
    clang-capf
    clang-format+
    irony
    irony-eldoc
    flycheck
    rtags
    realgud-lldb
    cmake-ide
    cmake-mode
    multiple-cursors
    elpy
    elpygen
    auctex
    company
    company-rtags
    company-irony
    rainbow-delimiters
    clang-format
    flycheck-popup-tip
    popup-kill-ring
    blacken
    ein
    conda
    magit
    importmagic
    pyimpsort
    pdf-tools
    reformatter
    )
  )

(mapc #'(lambda (package)
	  (unless (package-installed-p package)
	    (package-install package)))
      myPackages)
(menu-bar-mode -1)
(tool-bar-mode -1)
(if (display-graphic-p)
    (load-theme 'deeper-blue))
(load-file "~/.emacs.d/move-line-regions.el")
(load-file "~/.emacs.d/java-emacs.el")
;; (setq confirm-kill-processes nil)

;; (require 'auto-complete-clang)
(require 'realgud-lldb)
;; (require 'irony)
;; (require 'flycheck)
;; (require 'yasnippet)
(require 'rtags)
(cmake-ide-setup)

(global-set-key "\C-c\C-v" 'compile)
(global-set-key (kbd "<f5>") 'realgud--lldb)
(global-set-key (kbd "<f12>") 'rtags-find-symbol)
(setq inhibit-splash-screen t)
(setq line-number-mode t)
(setq column-number-mode t)
(setq font-lock-maximum-decoration t)
(global-linum-mode 1)
(display-time)
(global-font-lock-mode t)
;; See whitespaces and tabs
;; (require 'whitespace)
(setq whitespace-style '(space-mark))
(setq whitespace-display-mappings '((space-mark 32 [46])
				    (newline-mark 10 [10 182])))
(defun comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region."
  (interactive)
  (let (beg end)
    (if (region-active-p)
	(setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)))
(global-set-key (kbd "C-c c") 'comment-or-uncomment-region-or-line)



(require 'multiple-cursors)
(global-set-key (kbd "C-|") 'mc/edit-lines)
(global-set-key (kbd "C-c C-x C-c") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
(define-key mc/keymap (kbd "<return>") nil)

(fset 'skip-paragraph (kbd "M-C-f <down> C-a"))
(fset 'return-paragraph (kbd "M-C-b <up> C-a"))

;;Python Support
;; for help with environments: https://stackoverflow.com/questions/55175916/emacs-and-conda-workaround
;; (require 'elpy)
(elpy-enable)
(highlight-indentation-mode)
(setq elpy-rpc-virtualenv-path 'current)

;; Disable some annoying keybindigs that elpy tries to overwrite on us
(eval-after-load "elpy"
  '(cl-dolist (key '("M-<down>""M-<up>" ;; These are too good for just elpy, I copy them down
		     "M-<left>" "M-<right>"))
     (define-key elpy-mode-map (kbd key) nil)))

(global-set-key (kbd "C-M-<right>") 'elpy-nav-indent-shift-right)
(global-set-key (kbd "C-M-<left>") 'elpy-nav-indent-shift-left)



(setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
(add-hook 'elpy-mode-hook
	  (lambda()
	    (let ((col (current-column)))
	    (importmagic-mode)
	    (add-hook 'before-save-hook 'importmagic-fix-imports)
	    (add-hook 'before-save-hook 'blacken-buffer)
	    ;; (add-hook 'before-save-hook 'pyimpsort-buffer)
	    (move-to-column col)
	    )))
;; Use IPython for REPL
(setq elpy-shell-echo-output nil)
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
;; end of Python support


;; LaTeX support
;; (require 'tex)
(setq TeX-view-program-selection '((output-pdf "PDF Tools")))
(setq TeX-source-correlate-start-server t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

;; (require 'reftex)
(setq-default c-basic-offset 4)

					; revert pdf-view after compilation
(load "auctex.el" nil t t)
(add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
(add-hook 'latex-mode-hook 'flyspell-mode)
(add-hook 'latex-mode-hook 'flyspell-buffer)
(add-hook 'latex-mode-hook 'turn-on-reftex)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; end of LaTeX support


(defvar all-gud-modes
  '(gud-mode comint-mode gdb-locals-mode gdb-frames-mode  gdb-breakpoints-mode)
  "A list of modes when using gdb.")
(defun kill-gdb ()
  "Kill all gud buffers including Debugger, Locals, Frames, Breakpoints.
Do this after `q` in Debugger buffer."
  (interactive)
  (save-excursion
    (let ((count 0))
      (dolist (buffer (buffer-list))
	(set-buffer buffer)
	(when (member major-mode all-gud-modes)
	  (setq count (1+ count))
	  (kill-buffer buffer)
	  (delete-other-windows))) ;; fix the remaining two windows issue
      (message "Killed %i buffer(s)." count))))

;; ADDED BY DREW

;; Sets our compilation key binding to C-c C-v
;; (global-set-key "\C-c\C-v" 'compile)
;; Automatically set compilation mode to
;; move to an error when you move the point over it
;; Dont want this behavior? commend out the next 4 lines.
(add-hook 'compilation-mode-hook
	  (lambda ()
	    (progn
	      (next-error-follow-minor-mode))))

;;Automatically go to the first error
;;Dont want this behavior? comment out next line
(setq compilation-auto-jump-to-first-error t)


;;This makes rainbow delimiters mode the default.
;;comment out to turn it off.
(add-hook 'find-file-hook 'rainbow-delimiters-mode-enable)

;;Want electric pair mode? Uncomment the next line
(electric-pair-mode)

;;Want to turn off show paren mode? Comment out the below line.
(show-paren-mode)

;; these change the colors for rainbow delimters.   Dont' change them here!
;; change them in M-x customize
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-latex-sectioning-2-face ((t (:inherit font-latex-sectioning-3-face :height 1))))
 '(font-latex-sectioning-3-face ((t (:inherit font-latex-sectioning-4-face :height 1))))
 '(rainbow-delimiters-depth-1-face ((t (:inherit rainbow-delimiters-base-face :foreground "dark cyan"))))
 '(rainbow-delimiters-depth-2-face ((t (:inherit rainbow-delimiters-base-face :foreground "light green"))))
 '(rainbow-delimiters-depth-3-face ((t (:inherit rainbow-delimiters-base-face :foreground "yellow3"))))
 '(rainbow-delimiters-depth-4-face ((t (:inherit rainbow-delimiters-base-face :foreground "goldenrod2"))))
 '(rainbow-delimiters-depth-5-face ((t (:inherit rainbow-delimiters-base-face :foreground "tomato3"))))
 '(rainbow-delimiters-depth-6-face ((t (:inherit rainbow-delimiters-base-face :foreground "yellow2"))))
 '(rainbow-delimiters-unmatched-face ((t (:inherit rainbow-delimiters-base-face :background "red" :foreground "black")))))

;;This prevents gdb from getting a dedicated window,
;;which is generally super annoying
(defun set-window-undedicated-p (window flag)
  "Never set WINDOW dedicated.  WINDOW FLAG."
  flag)
(advice-add 'set-window-dedicated-p :override #'set-window-undedicated-p)


;;Autocompletion from company
;; (require 'company)
;; (require 'company-rtags)
;; (require 'company-clang)
;; This turns on autocomplete globally. Want to turn it off (why???) comment out next line.
(global-company-mode)
(add-to-list 'company-backends 'company-irony)

;;clang-format to format file reasonably.  Do NOT turn off!
;; (require 'clang-format)
(global-set-key [C-M-tab] 'clang-format-region)
(add-hook 'c-mode-common-hook
          (function (lambda ()
		      (add-hook 'write-contents-functions
				(lambda() (progn (clang-format-buffer) nil))))))

(add-hook 'cpp-mode-common-hook
          (function (lambda ()
                      (add-hook 'write-contents-functions
                                (lambda() (progn (clang-format-buffer) nil))))))

;; Flycheck: show you whats wrong while you write
;; Dont want?  comment out next 3 lines.
;; Dont want popups with whats wrong?  comment out third line only
(global-flycheck-mode)
(flycheck-popup-tip-mode)
(add-hook 'c++-mode-hook
	  (lambda() (setq flycheck-clang-language-standard "c++17")))
(add-hook 'c++-mode-hook
          (lambda () (setq flycheck-clang-include-path
                           (list (expand-file-name "~/opt/miniconda3/include/")))))

;; This does the popup menu for M-y.
;; if you dont want it, you can comment out the next 12 lines.
;; (require 'popup-kill-ring)
(defun drew-popup-kill-ring(&optional arg)
  "Set up popup menu.  ARG."
  (interactive "*p")
  (if  (eq last-command 'yank)
      (let ((inhibit-read-only t)
            (before (< (point) (mark t))))
        (if before
            (funcall (or yank-undo-function 'delete-region) (point) (mark t))
          (funcall (or yank-undo-function 'delete-region) (mark t) (point)))))
  (popup-kill-ring))
(global-set-key "\M-y" 'drew-popup-kill-ring)
(setq popup-kill-ring-interactive-insert t)

;;company mode doesnt play well with gdb mode.  Leave this here
(add-hook 'gud-mode-hook (lambda() (company-mode 0)))
(put 'upcase-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-command "latex")
 '(TeX-command-list
   (quote
    (("TeX" "%(PDF)%(tex) %(file-line-error) %`%(extraopts) %S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (plain-tex-mode texinfo-mode ams-tex-mode)
      :help "Run plain TeX")
     ("LaTeX" "%`%l%(mode)%' %T" TeX-run-TeX nil
      (latex-mode doctex-mode)
      :help "Run LaTeX")
     ("Makeinfo" "makeinfo %(extraopts) %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with Info output")
     ("Makeinfo HTML" "makeinfo %(extraopts) --html %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with HTML output")
     ("AmSTeX" "amstex %(PDFout) %`%(extraopts) %S%(mode)%' %t" TeX-run-TeX nil
      (ams-tex-mode)
      :help "Run AMSTeX")
     ("ConTeXt" "%(cntxcom) --once --texutil %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt once")
     ("ConTeXt Full" "%(cntxcom) %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt until completion")
     ("BibTeX" "bibtex %s" TeX-run-BibTeX nil
      (plain-tex-mode latex-mode doctex-mode context-mode texinfo-mode ams-tex-mode)
      :help "Run BibTeX")
     ("Biber" "biber %s" TeX-run-Biber nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Run Biber")
     ("View" "%V" TeX-run-discard-or-function t t :help "Run Viewer")
     ("Print" "%p" TeX-run-command t t :help "Print the file")
     ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command)
     ("File" "%(o?)dvips %d -o %f " TeX-run-dvips t
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Generate PostScript file")
     ("Dvips" "%(o?)dvips %d -o %f " TeX-run-dvips nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Convert DVI file to PostScript")
     ("Dvipdfmx" "dvipdfmx %d" TeX-run-dvipdfmx nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Convert DVI file to PDF with dvipdfmx")
     ("Ps2pdf" "ps2pdf %f" TeX-run-ps2pdf nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Convert PostScript file to PDF")
     ("Glossaries" "makeglossaries %s" TeX-run-command nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Run makeglossaries to create glossary
     file")
     ("Index" "makeindex %s" TeX-run-index nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Run makeindex to create index file")
     ("upMendex" "upmendex %s" TeX-run-index t
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Run upmendex to create index file")
     ("Xindy" "texindy %s" TeX-run-command nil
      (plain-tex-mode latex-mode doctex-mode texinfo-mode ams-tex-mode)
      :help "Run xindy to create index file")
     ("Check" "lacheck %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for correctness")
     ("ChkTeX" "chktex -v6 %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for common mistakes")
     ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document")
     ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files")
     ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files")
     ("Other" "" TeX-run-command t t :help "Run an arbitrary command"))))
 '(company-clang-arguments (quote ("-I/Users/pabloortiz/opt/miniconda3/include/")))
 '(elpy-modules
   (quote
    (elpy-module-company elpy-module-eldoc elpy-module-flymake elpy-module-pyvenv elpy-module-yasnippet elpy-module-django elpy-module-sane-defaults)))
 '(package-selected-packages
   (quote
    (realgud-lldb company-irony company-c-headers clang-capf rtags-xref irony irony-eldoc auto-complete-clang cmake-ide ein pyimpsort importmagic blacken conda py-autopep8 magit pdf-tools clang-format+ auctex rainbow-delimiters popup-kill-ring multiple-cursors flycheck-popup-tip company-rtags clang-format)))
 '(safe-local-variable-values (quote ((TeX-master . t)))))

(provide 'init)

;; If elpy is not imported, import these
(defun move-text-internal (arg)
   (cond
    ((and mark-active transient-mark-mode)
     (if (> (point) (mark))
            (exchange-point-and-mark))
     (let ((column (current-column))
              (text (delete-and-extract-region (point) (mark))))
       (forward-line arg)
       (move-to-column column t)
       (set-mark (point))
       (insert text)
       (exchange-point-and-mark)
       (setq deactivate-mark nil)))
    (t
     (beginning-of-line)
     (when (or (> arg 0) (not (bobp)))
       (forward-line)
       (when (or (< arg 0) (not (eobp)))
            (transpose-lines arg))
       (forward-line -1)))))
(defun move-text-down (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines down."
   (interactive "*p")
   (let ((col (current-column)))
     (move-beginning-of-line 1)
     ;; (elpy-nav-move-line-or-region-down)
     (move-text-internal arg)
     (move-to-column col)
     ))
(defun move-text-up (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines up."
   (interactive "*p")
   (let ((col (current-column)))
     (move-beginning-of-line 1)
     ;; (elpy-nav-move-line-or-region-up)
     (move-text-internal (- arg))
     (move-to-column col)
     ))

;;; init.el ends here
